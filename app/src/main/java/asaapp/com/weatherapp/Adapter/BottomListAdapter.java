package asaapp.com.weatherapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import asaapp.com.weatherapp.HelperMethod.SaveImageToCache;
import asaapp.com.weatherapp.Model.DailyWeather;
import asaapp.com.weatherapp.OnCustomItemClickListener;
import asaapp.com.weatherapp.R;

/**
 * Created by ayesandaaung on 28/4/17.
 */
public class BottomListAdapter extends RecyclerView.Adapter {
    private List<DailyWeather> list = Collections.emptyList();
    private Context mContext;
    int selectedPosition=-1;
    private OnCustomItemClickListener mCustomItemClickListener;

    public BottomListAdapter(List<DailyWeather> list, Context context) {
        this.list = list;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_tabbar, parent, false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder myViewHolder = (MyViewHolder)holder;
        DailyWeather dailyWeather = list.get(position);

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dayOfTheWeek = "";
        try {
            Date date = format.parse(SaveImageToCache.getInstance().convertTimeUnitToDate(dailyWeather.getDatetime()));
            dayOfTheWeek = (String) DateFormat.format("EEEE", date); // Thursday
        }catch (Exception ex){

        }
        myViewHolder.tv_day.setText(dayOfTheWeek);
        SaveImageToCache.getInstance().loadBitmap(dailyWeather.getIcon(),myViewHolder.img_weather_icon);
        int temp_min = SaveImageToCache.getInstance().convertToAbsoluteValue(dailyWeather.getTemp_min());
        int temp_max = SaveImageToCache.getInstance().convertToAbsoluteValue(dailyWeather.getTemp_max());
        myViewHolder.tv_description.setText(dailyWeather.getDescription());
        myViewHolder.tv_minmax_temperature.setText(temp_min+"°" + " / " + temp_max+"°");

        if(selectedPosition==position)
            holder.itemView.setBackgroundColor(Color.LTGRAY);
        else
            holder.itemView.setBackgroundColor(Color.parseColor("#ff33b5e5"));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCustomItemClickListener.onItemClicked(position);
                selectedPosition=position;
                notifyDataSetChanged();
            }
        });
    }

    public void setOnCustomListItemClickListener(OnCustomItemClickListener onCustomItemClickListener) {
        this.mCustomItemClickListener = onCustomItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_day;
        private ImageView img_weather_icon;
        private TextView tv_description;
        private TextView tv_minmax_temperature;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_day = (TextView) itemView.findViewById(R.id.tv_day);
            img_weather_icon = (ImageView) itemView.findViewById(R.id.img_weather_icon);
            tv_description = (TextView) itemView.findViewById(R.id.tv_description);
            tv_minmax_temperature = (TextView) itemView.findViewById(R.id.tv_minmax_temperature);

        }
    }
}
