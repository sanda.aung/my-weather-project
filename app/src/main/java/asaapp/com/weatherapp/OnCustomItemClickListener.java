package asaapp.com.weatherapp;

/**
 * Created by ayesandaaung on 2/5/17.
 */
public interface OnCustomItemClickListener {
    void onItemClicked(int adapterPosition);
}
