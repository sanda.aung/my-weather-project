package asaapp.com.weatherapp;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initScreen();
    }

    private void initScreen() {
        // Creating the ViewPager container fragment once
        MainFragment mainFragment = new MainFragment();
        int action = getIntent().getIntExtra("Action", 0);
        Bundle b = new Bundle();
        b.putInt("Action", action);
        mainFragment.setArguments(b);

        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, mainFragment).commit();
    }
}
