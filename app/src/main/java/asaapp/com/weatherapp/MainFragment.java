package asaapp.com.weatherapp;


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import asaapp.com.weatherapp.Adapter.BottomListAdapter;
import asaapp.com.weatherapp.HelperMethod.JSONParser;
import asaapp.com.weatherapp.HelperMethod.SaveImageToCache;
import asaapp.com.weatherapp.HelperMethod.WeatherAPI;
import asaapp.com.weatherapp.Model.CityModel;
import asaapp.com.weatherapp.Model.DailyWeather;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private RecyclerView lst_daily_weather;

    private TextView tv_city_name, tv_date, tv_temperature, tv_minmax_temperature, tv_description,tv_pressure,tv_humid,tv_clouds,tv_speed,tv_rain, tv_map ;
    private ImageView img_weather_icon;

    private List<CityModel> cityList;
    private String queryCity = "Singapore";

    public MainFragment() {
        // Required empty public constructor
    }
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            setRetainInstance(true);
            return;
        }
        setHasOptionsMenu(true);
    }

//    private void retrieveCityList(){
//        try {
//            JSONArray cityListArray = new JSONArray(JSONParser.loadJSONFromAsset(getActivity()));
//            cityList = new ArrayList<>();
//            for(int i=0; i< cityListArray.length(); i++){
//                JSONObject city = cityListArray.getJSONObject(i);
//                cityList.add(new CityModel(city.getInt("id"), city.getString("name"), city.getString("country"), city.getJSONObject("coord").getDouble("lon"), city.getJSONObject("coord").getDouble("lat")));
//            }
//            Log.e("CityCount", cityList.size()+"");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        tv_city_name = (TextView) view.findViewById(R.id.tv_city_name);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        tv_description = (TextView) view.findViewById(R.id.tv_description);
        tv_temperature = (TextView) view.findViewById(R.id.tv_temperature);
        tv_minmax_temperature = (TextView) view.findViewById(R.id.tv_minmax_temperature);
        tv_pressure = (TextView) view.findViewById(R.id.tv_pressure);
        tv_humid = (TextView) view.findViewById(R.id.tv_humid);
        tv_clouds = (TextView) view.findViewById(R.id.tv_clouds);
        tv_speed = (TextView) view.findViewById(R.id.tv_speed);
        tv_rain = (TextView) view.findViewById(R.id.tv_rain);
        tv_map = (TextView) view.findViewById(R.id.tv_map);

        img_weather_icon = (ImageView) view.findViewById(R.id.img_weather_icon);
        lst_daily_weather = (RecyclerView) view.findViewById(R.id.lst_daily_weather);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        lst_daily_weather.setLayoutManager(layoutManager);

        lst_daily_weather.addItemDecoration(
                new DividerItemDecoration(getContext(), layoutManager.getOrientation()){
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view);
                // hide the divider for the last child
                if (position == parent.getAdapter().getItemCount() - 1) {
                    outRect.setEmpty();
                } else {
                    super.getItemOffsets(outRect, view, parent, state);
                }
            }
        });

        retrieveData("Singapore");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item,menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                retrieveData(query);
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                //got to ....
                return true;

            case R.id.action_search:
                //show searchbar
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public void retrieveData(String query){
        // JSONObject a = JSONParser.getJSONFromUrl(String.format("%s=%s", serverurl, header));
        queryCity = query.trim().toLowerCase().equals("") ? "Singapore" : query;
        new AsyncTask<Void,Void,JSONObject>(){
            @Override
            protected JSONObject doInBackground(Void... voids) {
                //London&mode=xml&units=metric&cnt=7
                JSONObject a = JSONParser.getJSONFromUrl(WeatherAPI.SEVENDAY_URL+ queryCity + "&mode=json&units=metric&cnt=7&APPID="+ WeatherAPI.API_KEY );
                return a;
            }

            @Override
            protected void onPostExecute(JSONObject a) {
                super.onPostExecute(a);
                Log.e("Print", a.toString());

                if(a == null){
                    showAlertDialog();
                }else {
                    getWeatherInfo(a);
                }

            }
        }.execute();
    }

    public void showAlertDialog(){
        new AlertDialog.Builder(getContext())
                .setMessage("No weather data found for this country. Please try again!")
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                }).show();
    }

    public void getWeatherInfo(JSONObject a) {
        try {
            /*JSONObject weatherForecast = a.getJSONObject("channel").getJSONObject("item").getJSONObject("weatherForecast");
            JSONObject weatherForecast = (JSONObject) a.getJSONObject("channel").get("item");
            Log.e("weatherForecast",weatherForecast.getJSONObject("weatherForecast").getJSONArray("area").toString()); */

            JSONArray weatherArray = a.getJSONArray("list");
            JSONObject cityObject = a.getJSONObject("city");
            CityModel cityModel = new CityModel(cityObject.getInt("id"),cityObject.getString("name"),cityObject.getString("country"),cityObject.getJSONObject("coord").getDouble("lon"),cityObject.getJSONObject("coord").getDouble("lat"));
            List<DailyWeather> daily_weatherList = new ArrayList<DailyWeather>();
            for (int i = 0; i < weatherArray.length(); i++) {
                JSONObject temperatureObj = weatherArray.getJSONObject(i).getJSONObject("temp");
                JSONObject weatherObj = weatherArray.getJSONObject(i).getJSONArray("weather").getJSONObject(0);
                daily_weatherList.add(new DailyWeather(weatherObj.getString("main"), weatherObj.getString("description"), weatherObj.getString("icon"),
                        temperatureObj.getString("day"),weatherArray.getJSONObject(i).getString("pressure"),weatherArray.getJSONObject(i).getString("humidity"),
                        temperatureObj.getString("min"), temperatureObj.getString("max"), weatherArray.getJSONObject(i).getString("speed"),weatherArray.getJSONObject(i).getString("deg"),
                        weatherArray.getJSONObject(i).getString("clouds"),weatherArray.getJSONObject(i).has("rain") ? weatherArray.getJSONObject(i).getString("rain") : "",weatherArray.getJSONObject(i).getString("dt")));

            }
            setupData(daily_weatherList,cityModel);
        } catch (Exception e) {
            Log.e("ExceptionMsg", e.getMessage());
            e.printStackTrace();
        }
    }

    public void setupData(final List<DailyWeather> daily_weatherList, CityModel cityModel){

        tv_city_name.setText(cityModel.getName());
        tv_date.setText(SaveImageToCache.getInstance().convertTimeUnitToDate(daily_weatherList.get(0).getDatetime()));
        SaveImageToCache.getInstance().loadBitmap(daily_weatherList.get(0).getIcon(),img_weather_icon);
        tv_description.setText(daily_weatherList.get(0).getDescription());
        tv_temperature.setText(SaveImageToCache.getInstance().convertToAbsoluteValue(daily_weatherList.get(0).getTemp()) + "°C");
        tv_minmax_temperature.setText(SaveImageToCache.getInstance().convertToAbsoluteValue(daily_weatherList.get(0).getTemp_min())+"°" + "/" + SaveImageToCache.getInstance().convertToAbsoluteValue(daily_weatherList.get(0).getTemp_max())+"°" );
        tv_pressure.setText(daily_weatherList.get(0).getPressure());
        tv_humid.setText(daily_weatherList.get(0).getHumidity());
        tv_clouds.setText(daily_weatherList.get(0).getClouds());
        tv_speed.setText(daily_weatherList.get(0).getWind_speed());
        tv_rain.setText(daily_weatherList.get(0).getRain());

        BottomListAdapter adapter = new BottomListAdapter(daily_weatherList, getContext());
        lst_daily_weather.setAdapter(adapter);
        adapter.setOnCustomListItemClickListener(new OnCustomItemClickListener() {
            @Override
            public void onItemClicked(int adapterPosition) {
                DailyWeather dailyWeather = daily_weatherList.get(adapterPosition);
                tv_date.setText(SaveImageToCache.getInstance().convertTimeUnitToDate(dailyWeather.getDatetime()));
                SaveImageToCache.getInstance().loadBitmap(dailyWeather.getIcon(),img_weather_icon);
                tv_description.setText(dailyWeather.getDescription());
                tv_temperature.setText(SaveImageToCache.getInstance().convertToAbsoluteValue(dailyWeather.getTemp()) + "°C");
                tv_minmax_temperature.setText(SaveImageToCache.getInstance().convertToAbsoluteValue(dailyWeather.getTemp_min()) +"°"+ "/" + SaveImageToCache.getInstance().convertToAbsoluteValue(dailyWeather.getTemp_max())+"°" );
                tv_pressure.setText(dailyWeather.getPressure());
                tv_humid.setText(dailyWeather.getHumidity());
                tv_clouds.setText(dailyWeather.getClouds());
                tv_speed.setText(dailyWeather.getWind_speed());
                tv_rain.setText(dailyWeather.getRain());
            }
        });
        adapter.notifyDataSetChanged();

    }


}
