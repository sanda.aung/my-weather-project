package asaapp.com.weatherapp.HelperMethod;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

//https://developer.android.com/topic/performance/graphics/cache-bitmap.html
public class SaveImageToCache {
    private static SaveImageToCache instance = null;
    private LruCache<String, Bitmap> mMemoryCache;

    public SaveImageToCache(){
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public static SaveImageToCache getInstance() {
        if (instance == null)
            instance = new SaveImageToCache();
        return instance;
    }

    public void loadBitmap(String resId, final ImageView imageView) {
        //final String imageKey = String.valueOf(resId);
        final String imageKey = resId;
        final Bitmap bitmap = getBitmapFromMemCache(imageKey);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            new AsyncTask<String,Void,Bitmap>(){
                @Override
                protected Bitmap doInBackground(String... params) {

                    String urldisplay = "http://openweathermap.org/img/w/"+params[0]+".png";
                    Bitmap mIcon11 = null;
                    try {
                        InputStream in = new java.net.URL(urldisplay).openStream();
                        mIcon11 = BitmapFactory.decodeStream(in);
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }

                    addBitmapToMemoryCache(String.valueOf(params[0]), mIcon11);
                    return mIcon11;
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    imageView.setImageBitmap(bitmap);
                }
            }.execute(resId);
        }
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public String convertTimeUnitToDate(String time){
        // long unixSeconds = 1372339860;
        long unixSeconds = Long.parseLong(time);
        Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public int convertToAbsoluteValue(String temp){
        Double value = Double.parseDouble(temp);
        return value.intValue();
    }
}
