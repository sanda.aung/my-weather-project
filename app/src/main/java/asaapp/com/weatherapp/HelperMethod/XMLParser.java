package asaapp.com.weatherapp.HelperMethod;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import asaapp.com.weatherapp.Model.WeatherModel;

/**
 * Created by user on 8/8/16.
 */
public class XMLParser {

    public volatile boolean parsingComplete = true;
    private WeatherModel model = new WeatherModel();
    private XmlPullParserFactory xmlFactoryObject;
    // constructor
    public XMLParser() {

    }

    public WeatherModel getModel() {
        return model;
    }

    public void setModel(WeatherModel model) {
        this.model = model;
    }

    public void parseXMLAndStoreIt(XmlPullParser myParser) {
        int event;
        String text=null;

        try {
            event = myParser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {
                String name=myParser.getName();
                switch (event){
                    case XmlPullParser.START_TAG:
                        break;

                    case XmlPullParser.TEXT:
                        text = myParser.getText();
                        break;

                    case XmlPullParser.END_TAG:
//                        if(name.equals("country")){
//                            country = text;
//                        }

                        if(name.equals("forecast")){
                            model.setForecast(myParser.getAttributeValue(null,"value"));
                        }
                        else if(name.equals("lat")){
                            model.setLat(myParser.getAttributeValue(null,"value"));
                        }

                        else if(name.equals("lon")){
                            model.setLon(myParser.getAttributeValue(null,"value"));
                        }

                        else if(name.equals("name")){
                            model.setName(myParser.getAttributeValue(null,"value"));
                        }
                        else{
                        }
                        break;
                }
                event = myParser.next();
                this.setModel(model);
            }
            parsingComplete = false;
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchXML(final String apiUrl){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    URL url = new URL(apiUrl);
                    HttpURLConnection conn = (HttpURLConnection)url.openConnection();

                    conn.setReadTimeout(10000 /* milliseconds */);
                    conn.setConnectTimeout(15000 /* milliseconds */);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();

                    InputStream stream = conn.getInputStream();
                    xmlFactoryObject = XmlPullParserFactory.newInstance();
                    XmlPullParser myparser = xmlFactoryObject.newPullParser();

                    myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                    myparser.setInput(stream, null);

                    parseXMLAndStoreIt(myparser);
                    stream.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
