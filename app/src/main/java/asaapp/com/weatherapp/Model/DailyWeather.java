package asaapp.com.weatherapp.Model;

/**
 * Created by user on 10/8/16.
 */
public class DailyWeather {

    //weather
    private String main;
    private String description;
    private String icon;

    //main
    private String datetime;
    private String temp;
    private String temp_min;
    private String temp_max;
    private String pressure;
    private String humidity;

    private String wind_speed;
    private String wind_deg;
    private String clouds;
    private String rain;

    public DailyWeather(){

    }

    public DailyWeather(String main, String description, String icon){
        this.main = main;
        this.description = description;
        this.icon = icon;
    }

    public DailyWeather(String main, String description, String icon, String temp, String pressure, String humidity, String temp_min, String temp_max, String wind_speed, String wind_deg, String clouds,String rain, String datetime){
        this.main = main;
        this.description = description;
        this.icon = icon;
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.humidity = humidity;
        this.wind_speed = wind_speed;
        this.wind_deg = wind_deg;
        this.clouds = clouds;
        this.rain = rain;
        this.datetime = datetime;
    }


    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }



    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(String wind_speed) {
        this.wind_speed = wind_speed;
    }

    public String getWind_deg() {
        return wind_deg;
    }

    public void setWind_deg(String wind_deg) {
        this.wind_deg = wind_deg;
    }

    public String getClouds() {
        return clouds;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getRain() {
        return rain;
    }

    public void setRain(String rain) {
        this.rain = rain;
    }

}
