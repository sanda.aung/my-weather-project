package asaapp.com.weatherapp.Model;

/**
 * Created by user on 8/8/16.
 */
public class WeatherModel {

   // <area forecast="PC" lat="1.37500000" lon="103.83900000" name="Ang Mo Kio"/>

    private String forecast;
    private String lat;
    private String lon;
    private String name;

    public WeatherModel(){

    }

    public WeatherModel(String forecast, String lat, String lon, String name ){
        this.forecast = forecast;
        this.lat = lat;
        this.lon = lon;
        this.name = name;
    }

    public String getForecast() {
        return forecast;
    }

    public void setForecast(String forecast) {
        this.forecast = forecast;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
